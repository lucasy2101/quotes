import style from "./App.module.scss";
import Card from "./components/Card/Card";
function App() {
  return (
    <div className={style.app}>
      <Card />
    </div>
  );
}

export default App;
