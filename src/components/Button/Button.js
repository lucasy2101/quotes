import "./Button.scss";
import axios from "axios";
const Button = ({ buttonName, color, changeRandomNumber}) => {

  return (
    <button className="button" style={{ backgroundColor: color }} onClick={changeRandomNumber}>
      {buttonName}
    </button>
  );
};
export default Button;
