import Button from "../Button/Button";
import "./Card.scss";
import { useEffect, useState } from "react";
import axios from "axios";
const Card = () => {
  const [quote, setQuote] = useState("");
  const [randomNumber, setRandomNumber] = useState(null);
  const getNewQuote = () => {
    axios.get("https://type.fit/api/quotes").then((res) => {
      setQuote(res);
      setRandomNumber(Math.floor(Math.random() * (res.data.length - 1)));
      console.log(res);
    });
  };
  useEffect(() => {
    getNewQuote();
  }, []);
  const changeRandomNumber = () => {
    const number = Math.floor(Math.random() * (quote.data.length - 1));
    setRandomNumber(number);
  };
  return (
    <div className="Container">
      {quote && (
        <>
          <div className="quote">
            <span style={{ fontSize: "80px" }}>"</span>
            <span>{quote.data[randomNumber].text}</span>
          </div>
          <p>{quote.data[randomNumber].author}</p>
        </>
      )}

      <div className="buttons">
        <Button buttonName="Twitter" color="blue" />
        <Button
          buttonName="New Quote"
          changeRandomNumber={changeRandomNumber}
        />
      </div>
    </div>
  );
};
export default Card;
